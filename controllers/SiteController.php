<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\TaskForm;
use app\models\Tasks;
use app\models\User;
use app\models\Users;
use yii\data\Pagination;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionTasks()
    {
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $taskList = Tasks::find()
            ->where([
                'user_id' => Yii::$app->user->id
            ]);
        $countQuery = clone $taskList;
        $totalCount = $countQuery->count();
        $pages = new Pagination(['totalCount' => $totalCount, 'pageSize' => 10]);
        $pages->pageSizeParam = false;
        $taskList = $taskList->offset($pages->offset)
            ->orderBy(['created_at'=> SORT_DESC])
            ->limit($pages->limit)
            ->all();

        $model = new Tasks();
        $model->user_id = 1;
        $model->title = "Задача ".($totalCount+1);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->refresh();
        }

        return $this->render('tasks', [
            "tasks" => $taskList,
            "pages" => $pages,
            "model" => $model
        ]);
    }


    public function actionIndex()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->actionTasks();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }


    public function actionLogout()
    {
        Yii::$app->user->logout();
        return $this->goHome();
    }

    public function actionProfile()
    {
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $user = User::findIdentity(Yii::$app->user->id);
        return $this->render('profile', [
            'user' => $user,
        ]);
    }



    public function actionResetpassword()
    {
        $response = [];
        $response['success'] = false;

        if (Yii::$app->user->isGuest) {
            $response['code'] = 1;
            $response['message'] = "You should login";
            echo json_encode($response);
            die();
        }


        if (Yii::$app->request->isAjax)
        {
            $passLength = 6;
            $symbols = '0123456789abcdefghijklmnopqrstuvwxyz';
            $newPassword = "";
            for ($i = 0; $i < $passLength; $i++)
                $newPassword .= $symbols[rand(0, strlen($symbols) - 1)];

            $user = Users::findOne(Yii::$app->user->id);
            if (!Yii::$app->mailer->compose()
                ->setFrom(Yii::$app->params['adminEmail'])
                ->setTo($user->username)
                ->setSubject('New password')
                ->setTextBody($newPassword)
                ->send())
            {
                $response['code'] = 3;
                $response['message'] = "Email error";
                echo json_encode($response);
            }

            $user->password = password_hash($newPassword, PASSWORD_BCRYPT);
            $user->save();
            $response['success'] = true;
            $response['new_password'] = $newPassword;

            echo json_encode($response);
            die();
        }
        $response['code'] = 2;
        $response['message'] = "Only ajax";
        echo json_encode($response);
    }
}
