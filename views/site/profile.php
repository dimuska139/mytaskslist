<?php
use yii\helpers\Html;

$this->title = 'Your profile';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-tasks">
    <h1><?= Html::encode($this->title) ?></h1>

    <h4>Your email: <?=$user->username;?></h4>
    <button class="btn btn-danger reset-password-button" >Reset password</button>
    <img class="loading-indicator" src="/img/loading.gif">
    <div class="error-field"></div>
    <div class="success-field"></div>
</div>
