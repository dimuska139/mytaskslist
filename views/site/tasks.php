<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\widgets\LinkPager;

$this->title = 'Tasks';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-tasks">
    <h1><?= Html::encode($this->title) ?></h1>
    <?php $form = ActiveForm::begin([
        'id' => 'login-form',
        'options' => ['class' => 'form-horizontal'],
        'fieldConfig' => [
            'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
            'labelOptions' => ['class' => 'col-lg-1 control-label'],
        ],
    ]); ?>

    <?= $form->field($model, 'title')->textInput(['autofocus' => true]) ?>

    <?= $form->field($model, 'description')->textArea(['rows' => '6']) ?>


    <div class="form-group">
        <div class="col-lg-offset-1 col-lg-11">
            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary', 'name' => 'save-button']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
    <?php if (!empty($tasks)):?>
        <?php
        echo LinkPager::widget([
            'pagination' => $pages,
        ]);
        ?>
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Title</th>
                    <th>Description</th>
                    <th>Created at</th>
                </tr>
            </thead>
            <tbody>
            <?php foreach ($tasks as $task): ?>
                <tr>
                    <td width="5%"><?=$task->id;?></td>
                    <td width="20%"><?=$task->title;?></td>
                    <td><?=$task->description;?></td>
                    <td width="10%">
                        <?=(new \DateTime($task->created_at))->format('Y-m-d');?>
                        <br>
                        <?=(new \DateTime($task->created_at))->format('H:i:s');?>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
        <?php
        echo LinkPager::widget([
            'pagination' => $pages,
        ]);
        ?>
    <?php endif; ?>
</div>
