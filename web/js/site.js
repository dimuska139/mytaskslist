/**
 * Created by dimuska139 on 11.10.16.
 */

$(document).ready(function(){
    $('.reset-password-button').on('click', function(){
        if (confirm("Are you sure?")){
            $.ajax({
                type: "post",
                url: "/site/resetpassword",
                dataType: "json",
                data: {},
                beforeSend: function(){
                    $('.error-field').html('');
                    $('.loading-indicator').show();
                },
                complete: function(){
                    $('.loading-indicator').hide();
                },
                success: function(response){
                    if (response.success)
                    {
                        $('.success-field').html('Your new password: '+response.new_password);
                    } else {
                        $('.error-field').html('Something went wrong');
                    }
                }
            });
        }
    });
});